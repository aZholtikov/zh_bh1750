/**
 * @file
 * Header file for the zh_bh1750 component.
 *
 */

#pragma once

#include "esp_err.h"
#include "esp_log.h"
#include "driver/i2c.h"
#ifdef CONFIG_IDF_TARGET_ESP8266
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#endif

/**
 * @brief Default values for zh_bh1750_init_config_t structure for initial initialization of the sensor.
 *
 */
#define ZH_BH1750_INIT_CONFIG_DEFAULT()    \
    {                                      \
        .i2c_address = I2C_ADDRESS_LOW,    \
        .operation_mode = HIGH_RESOLUTION, \
        .work_mode = ONE_TIME,             \
        .i2c_port = 0                      \
    }

#ifdef __cplusplus
extern "C"
{
#endif

    /**
     * @brief Enumeration of possible sensor operation mode.
     *
     */
    typedef enum
    {
        LOW_RESOLUTION,    ///< Measurement with 4 lx resolution.
        HIGH_RESOLUTION,   ///< Measurement with 1 lx resolution.
        HIGH_RESOLUTION_2, ///< Measurement with 0,5 lx resolution.
    } __attribute__((packed)) zh_bh1750_operation_mode_t;

    /**
     * @brief Enumeration of possible sensor work mode.
     *
     */
    typedef enum
    {
        CONTINUOUSLY, ///< Continuously measurement.
        ONE_TIME      ///< One time measurement. Sensor is power down after measurement.
    } __attribute__((packed)) zh_bh1750_work_mode_t;

    /**
     * @brief Enumeration of possible I2C sensor address.
     *
     */
    typedef enum
    {
        I2C_ADDRESS_HIGH = 0x5C, ///< Data pin is connected to VCC.
        I2C_ADDRESS_LOW = 0x23   ///< Data pin is not connected / connected to GND.
    } __attribute__((packed)) zh_bh1750_i2c_address_t;

    /**
     * @brief Structure for initial initialization of the sensor.
     *
     * @note Before initialize the sensor recommend initialize zh_bh1750_init_config_t structure with default values.
     *
     * @code
     *       zh_bh1750_init_config_t config = ZH_BH1750_INIT_CONFIG_DEFAULT()
     * @endcode
     *
     */
    typedef struct
    {
        zh_bh1750_i2c_address_t i2c_address;       ///< Sensor addresss. @note
        zh_bh1750_operation_mode_t operation_mode; ///< Operation mode. @note
        zh_bh1750_work_mode_t work_mode;           ///< Work mode. @note
        bool i2c_port;                             ///< I2C port. @note
    } __attribute__((packed)) zh_bh1750_init_config_t;

    /**
     * @brief      Initialize BH1750 sensor.
     *
     * @param[in]  config  Pointer to BH1750 initialized configuration structure. Can point to a temporary variable.
     *
     * @attention   I2C driver must be initialized first.
     *
     * @note Before initialize the sensor recommend initialize zh_bh1750_init_config_t structure with default values.
     *
     * @code
     *       zh_bh1750_init_config_t config = ZH_BH1750_INIT_CONFIG_DEFAULT()
     * @endcode
     *
     * @return
     *              - ESP_OK if initialization was success
     *              - ESP_ERR_INVALID_ARG if parameter error
     */
    esp_err_t zh_bh1750_init(zh_bh1750_init_config_t *config);

    /**
     * @brief      Read BH1750 sensor.
     *
     * @param[out]  data  Pointer for BH1750 sensor reading data.
     *
     * @return
     *              - ESP_OK if read was success
     *              - ESP_ERR_INVALID_ARG if parameter error
     *              - ESP_FAIL if sending command error or slave has not ACK the transfer
     *              - ESP_ERR_INVALID_STATE if I2C driver not installed or not in master mode
     *              - ESP_ERR_TIMEOUT if operation timeout because the bus is busy
     */
    esp_err_t zh_bh1750_read(float *data);

    /**
     * @brief      Adjust BH1750 sensor sensitivity.
     *
     * @param[in]  value  Value of sensitivity.
     *
     * @note       Range value from 31 to 254. 69 by default. Can be changed continuously during program work.
     *
     * @return
     *              - ESP_OK if adjust was success
     *              - ESP_ERR_INVALID_ARG if parameter error
     *              - ESP_FAIL if sending command error or slave has not ACK the transfer
     *              - ESP_ERR_INVALID_STATE if I2C driver not installed or not in master mode
     *              - ESP_ERR_TIMEOUT if operation timeout because the bus is busy
     */
    esp_err_t zh_bh1750_adjust(uint8_t value);

#ifdef __cplusplus
}
#endif